from curses import keyname
import sqlite3
from time import sleep
from datetime import datetime, date, time
import calendar
from sqlite3 import Error
import pandas as pd
import pytz
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt

mqttBroker = "broker"

daysofweek = {
    "monday": 6,
    "tuesday": 7,
    "wednesday": 8,
    "thursday": 9,
    "friday": 10,
    "saturday": 11,
    "sunday": 12,
}

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        print ("Successfully Connected")
    except Error as e:
        print(e)
    return conn

# def startdate(conn):
#     cur = conn.cursor()

def dayofweek(experience):
    now = datetime.now(pytz.timezone('US/Mountain'))
    curr_date = now.today()
    today = calendar.day_name[curr_date.weekday()]
    day = daysofweek[str.lower(today)]
    if experience[day]:
        return True
    else:
        return False
    

def startdate(experience):
    now = datetime.now(pytz.timezone('US/Mountain'))
    dt_string = now.strftime("%Y-%d-%m %H:%M:00") 
    # Check if the current time is past the start date set in the model
    if dt_string >= experience[3]:
        return True
    else:
        return False

def frequency(experience):
    
    now = datetime.now(pytz.timezone('US/Mountain'))
    current_time = now.strftime("%H:%M")
    starttime = experience[4]
    print ("start " + starttime)
    stoptime  = experience[5]
    print ("stop " + stoptime)
    frequency = experience[13]
    freq_list = pd.timedelta_range(start=f'1 day {starttime}', end=f'1 day {stoptime}', freq=f'{frequency}')
    
    for freq in freq_list:
        entryTime = time(freq.components.hours, freq.components.minutes)
        if (current_time == entryTime.strftime("%H:%M")):
            print ("triggered")
            return True
    return False

def select_all_experiences(conn):
    """
    Query all rows in the ScheduleExperience table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM home_scheduleexperience")

    # now = datetime.now()
    # dt_string = now.strftime("%Y-%d-%m %H:%M:00") 
    rows = cur.fetchall()
    return rows



def main():
    database = r"/code/data/db.sqlite3"

    # create a database connection
    conn = create_connection(database)
    with conn:
        print("connected experiences")
        while True:
            experiences = select_all_experiences(conn)
            for experience in experiences:
                print (experience)
                if startdate(experience) and dayofweek(experience) and frequency(experience):
                    client = mqtt.Client("Experience Manager")
                    client.connect(mqttBroker)
                    client.publish("Experience", str.lower(experience[1]))                    
                    print("Blinky")
            sleep(60) # Deplay read once a minute. Currently not exaclty at the minute mark


if __name__ == '__main__':
    main()