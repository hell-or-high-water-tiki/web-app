 ## Start the project
 ```bash
 docker-compose run web django-admin startproject hhwt .
 ```
 
If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up

```bash
docker-compose run web django-admin startapp home
```

set STATIC_ROOT
https://docs.djangoproject.com/en/4.0/howto/static-files/#deployment

```bash
docker-compose run web python collectstatic
```

## Connect Raspberry Pi to Wifi
https://www.makeuseof.com/connect-to-wifi-with-nmcli/