from django.forms import ModelForm
from tempus_dominus.widgets import DatePicker, TimePicker
from .models import ScheduleExperience

class SchedulerForm(ModelForm):
    class Meta:
        model = ScheduleExperience
        fields = ('__all__')
        widgets = {
            'startdate': DatePicker(
                options={
                    'useCurrent': False,
                    'collapse': False,
                },
                attrs={
                    'append': 'fa fa-calendar',
                    'icon_toggle': True,
                }
            ),  
            'starttime': TimePicker(
                options={
                    'enabledHours': [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
                    'defaultDate': '2022-01-01T14:00:00'
                },
                attrs={
                    'input_toggle': True,
                    'input_group': False,
                },
            ),
            'stoptime': TimePicker(
                options={
                    'enabledHours': [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
                    'defaultDate': '2022-01-01T14:00:00'
                },
                attrs={
                    'input_toggle': True,
                    'input_group': False,
                },
            ),
        }

