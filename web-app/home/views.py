from email import message
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import UpdateView
from django.contrib import messages

# from django.views.generic import ListView

from .forms import SchedulerForm
from .models import ScheduleExperience
import paho.mqtt.client as mqtt

# Grabbing from Docker internal DNS in docker-compose.yml service
mqttBroker = "broker"

# CALL MODEL OBJECTS FROM PROJECT
def home(request):
    return render (request, 'home/home.html')

def scheduler(request):
    scheduledexperiences = ScheduleExperience.objects.all()
    return render (request, 'home/scheduler.html', {'scheduledexperiences':scheduledexperiences})

def schedulenew(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SchedulerForm(request.POST, request.FILES)
        # check whether it's valid

        if form.is_valid():
            form.save()
            title = form.cleaned_data.get('title')
            messages.success(request, f'{title} experience scheduled successfully')
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return redirect('scheduler') #add thanks page here

    # if a GET (or any other method) we'll create a blank form
    else:
        form = SchedulerForm()

    return render(request, 'home/schedulenew.html', {'form': form})

def manual(request):
    experience = str(request.GET.get('Experience'))
    if experience == "blink":
        client = mqtt.Client("Experience Manager")
        client.connect(mqttBroker)
        client.publish("Experience", experience)
        print ("Blink LED in Pi")
    if experience == "Storm":
        print ("Start thunder and rain")

    return render (request, 'home/manual.html')
