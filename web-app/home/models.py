from email.policy import default
from django.db import models

# Create your models here.
## Django model fields
### https://docs.djangoproject.com/en/4.0/ref/models/fields/

"""
the keys to the following set match the pandas time_range paramters
for minutes and hours so that the pd.timedelta_rangecan easily 
read and determine frequency. 
"""
FREQUENCY_CHOICES = (
    ('15T', 'every 15 minutes'),
    ('30T', 'every 30 minutes'),
    ('1H', 'every hour'),
    ('2H', 'every 2 hours'),
    ('3H', 'every 3 hours'),
    ('4H', 'every 4 hours'),
    ('5H', 'every 5 hours'),
    ('6H', 'every 6 hours'),
    ('8H', 'every 8 hours'),

)

class ScheduleExperience(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=300)
    startdate = models.DateField()
    starttime = models.TimeField()
    stoptime = models.TimeField()
    monday = models.BooleanField()
    tuesday = models.BooleanField()
    wednesday = models.BooleanField()
    thursday = models.BooleanField()
    friday = models.BooleanField()
    saturday = models.BooleanField()
    sunday = models.BooleanField()
    frequency = models.CharField(choices=FREQUENCY_CHOICES, default="1", max_length=8)
    image = models.ImageField(upload_to='images/', blank=True)
    URL = models.URLField (blank=True)




    def __str__(self):
        return self.title

